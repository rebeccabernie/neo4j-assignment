# Neo4J Assignment  

> Module: Data Architecture and Database Systems  
> Author: Rebecca Kane A00258303  

## Online Bookstore

This graph database is based on a basic online bookstore system. Nodes in the database include Books, Categories, Customers, and Offers. The structure of the graph database is as follows -  
Book - [ BELONGS_TO ] - Category  
Offer - [ APPLIES_TO ] - Book or Category  
Customer - [ HAS BOUGHT ] - Book  

The main advantage of using a graph database as opposed to a relational database for this application is the ability to find patterns. For example, in this online bookstore Neo4J/Cypher allows the user to recommend a customer books from a category based on books they've already bought from that category, or find the purchases of customers under a certain age, which could be useful for recommendations or finding the most popular books within an age group.

The `graphSetup.txt` file can be copied in full and pasted into a Neo4J browser, and run in one query.  
The `ExampleQueries.txt`  file contains 17 sample queries for finding or updating information within the database.